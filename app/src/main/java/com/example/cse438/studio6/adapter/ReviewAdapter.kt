package com.example.cse438.studio6.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.cse438.studio6.R
import com.example.cse438.studio6.model.Review

class ReviewAdapter(context: Context, items: ArrayList<Review>): ArrayAdapter<Review>(context, R.layout.product_review_list_item, items) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(context).inflate(R.layout.product_review_list_item, parent, false)

        val review = getItem(position)

        view?.findViewById<TextView>(R.id.body)?.text = review?.getBody()

        var author = "- "
        author += if (review != null && review.getIsAnonymous()) {
            "Anonymous"
        } else {
            review?.getUsername()
        }
        view?.findViewById<TextView>(R.id.author)?.text = author

        return view
    }
}